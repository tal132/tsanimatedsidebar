#import "TSAnimatedSidebar.h"
#import "MenuView.h"
#import "JCRBlurView.h"


@interface TSAnimatedSidebar () <MenuViewDelegate>

@end

@implementation TSAnimatedSidebar
{
    MenuView* _menuView;
    JCRBlurView* _blurView;
}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _blurView = [JCRBlurView new];
    [_blurView setFrame:CGRectMake(380.0f,-100.0f,340.0f,650.0f)];
    self.view.clipsToBounds = YES;
    [self.view addSubview:_blurView];

    
    _menuView = [[[NSBundle mainBundle] loadNibNamed:@"MenuView" owner:self options:nil] objectAtIndex:0];
    _menuView.delegate = self;
    
    _menuView.menu1Label.text = self.menu1Label;
    _menuView.menu2Label.text = self.menu2Label;
    
    _menuView.menu1ImageView.image = [UIImage imageNamed:self.image1StartName];
    _menuView.menu2ImageView.image = [UIImage imageNamed:self.image2StartName];
    
    [self.view addSubview:_menuView];
    //164,56
    CGRect frame = _menuView.frame;
    frame.origin.x = 210;
    frame.origin.y = 16;
    _menuView.frame = frame;

}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self animateMenus];
}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    CGPoint locationPoint = [[touches anyObject] locationInView:self.view];
    UIView* view = [self.view hitTest:locationPoint withEvent:event];
    
    if (view == self.view)
    {
        [self.delegate menuItemTapped:-1];
        [self close];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)close
{
    [UIView animateWithDuration:1.0 * self.animationSpeedFactor animations:^{
        [_blurView setFrame:CGRectMake(380.0f,-100.0f,340.0f,650.0f)];
        CGRect frame = _menuView.frame;
        frame.origin.x += 180;
        _menuView.frame = frame;
        
    } completion:^(BOOL finished) {
        [self.delegate didFinishAnimatingOut];
    }];
}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)animateMenus
{
    [UIView animateKeyframesWithDuration:0.75 * self.animationSpeedFactor delay:0.25 * self.animationSpeedFactor options:UIViewKeyframeAnimationOptionBeginFromCurrentState animations:^{
        
        CGRect frame = _menuView.menu1ImageView.frame;
        frame.size.height = 40;
        frame.size.width = 40;
        frame.origin.x -= 16;
        frame.origin.y -= 16;
        _menuView.menu1ImageView.frame = frame;
        
    } completion:^(BOOL finished){
        _menuView.menu1ImageView.image = [UIImage imageNamed:@"flat_circle_final"];
    }];
    
    [UIView animateKeyframesWithDuration:0.75 * self.animationSpeedFactor delay:1.0 * self.animationSpeedFactor options:UIViewKeyframeAnimationOptionBeginFromCurrentState animations:^{
        
        CGRect frame = _menuView.menu1Label.frame;
        frame.origin.x -= 120;
        _menuView.menu1Label.frame = frame;
        
    } completion:nil];
    
    
    [UIView animateKeyframesWithDuration:0.75 * self.animationSpeedFactor delay:1.1 * self.animationSpeedFactor options:UIViewKeyframeAnimationOptionBeginFromCurrentState animations:^{
        
        CGRect frame = _menuView.menu2ImageView.frame;
        frame.size.height = 40;
        frame.size.width = 40;
        frame.origin.x -= 16;
        frame.origin.y -= 16;
        
        _menuView.menu2ImageView.frame = frame;
        
    } completion:^(BOOL finished){
        _menuView.menu2ImageView.image = [UIImage imageNamed:@"flat_circle_red_final"];
    }];
    
    [UIView animateKeyframesWithDuration:0.75 * self.animationSpeedFactor delay:1.25 * self.animationSpeedFactor options:UIViewKeyframeAnimationOptionBeginFromCurrentState animations:^{
        
        CGRect frame = _menuView.menu2Label.frame;
        frame.origin.x -= 120;
        _menuView.menu2Label.frame = frame;
        
    } completion:nil];
    
#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)
    double rads = DEGREES_TO_RADIANS(345);
    CGAffineTransform transform = CGAffineTransformRotate(CGAffineTransformIdentity, rads);
    
    [UIView animateWithDuration:1.0 * self.animationSpeedFactor animations:^{
        [_blurView setFrame:CGRectMake(200.0f,-100.0f,340.0f,650.0f)];
        _blurView.transform = transform;
    }];

    
}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)menuTapped:(NSInteger)index
{
    [self.delegate menuItemTapped:index];
    [self close];
    
}

@end
