#import <UIKit/UIKit.h>

@protocol TSAnimatedSidebarDelegate <NSObject>

//notify the delegate that a menu item has been tapped
- (void)menuItemTapped:(NSInteger)index;

//notify the delegate the side bar has finished animating itself out, so it can dispose of it
- (void)didFinishAnimatingOut;

@end

@interface TSAnimatedSidebar : UIViewController

@property (nonatomic,weak) id<TSAnimatedSidebarDelegate> delegate;

@property (nonatomic) float animationSpeedFactor; //this will control the speed of the animations

@property (nonatomic) NSString* image1StartName; //image that will be used as the first image shown for menu 1
@property (nonatomic) NSString* image2StartName; //image that will be used as the first image shown for menu 2
@property (nonatomic) NSString* image1FinalName; //image shown when the menu item is done expanding
@property (nonatomic) NSString* image2FinalName; //image shown when the menu item is done expanding

@property (nonatomic) NSString* menu1Label;
@property (nonatomic) NSString* menu2Label;



- (void)close;

@end
