#import <UIKit/UIKit.h>

//an image view subclass that responds to touch
@class ResponsiveImageView;

@protocol ResponsiveImageViewDelegate <NSObject>

- (void)imageTouched:(ResponsiveImageView*)imageView;

@end

@interface ResponsiveImageView : UIImageView
@property (nonatomic,weak) id<ResponsiveImageViewDelegate> delegate;
@end

//a uilabel subclass that responds to touch
@class ResponsiveLabel;

@protocol ResponsiveLabelDelegate <NSObject>

- (void)labelTouched:(ResponsiveLabel*)label;

@end

@interface ResponsiveLabel : UILabel
@property (nonatomic,weak) id<ResponsiveLabelDelegate> delegate;
@end

//the menu view
@protocol MenuViewDelegate <NSObject>

- (void)menuTapped:(NSInteger)index;

@end

@interface MenuView : UIView

@property (nonatomic,weak) id<MenuViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet ResponsiveImageView* menu1ImageView;
@property (weak, nonatomic) IBOutlet ResponsiveImageView *menu2ImageView;
@property (weak, nonatomic) IBOutlet ResponsiveLabel *menu1Label;
@property (weak, nonatomic) IBOutlet ResponsiveLabel *menu2Label;



@end
