#import "MenuView.h"


@implementation ResponsiveImageView

//////////////////////////////////////////////////////////////////////////////////////////

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [self.delegate imageTouched:self];
}

@end

//////////////////////////////////////////////////////////////////////////////////////////

@implementation ResponsiveLabel

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [self.delegate labelTouched:self];
}

@end

//////////////////////////////////////////////////////////////////////////////////////////


@interface MenuView() <ResponsiveImageViewDelegate, ResponsiveLabelDelegate>

@end

//////////////////////////////////////////////////////////////////////////////////////////
@implementation MenuView

//////////////////////////////////////////////////////////////////////////////////////////


- (void)imageTouched:(ResponsiveImageView *)imageView
{
    if (imageView == self.menu1ImageView)
    {
        [self.delegate menuTapped:1];
    }
    else
    {
        [self.delegate menuTapped:2];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)labelTouched:(ResponsiveLabel *)label
{
    if (label == self.menu1Label)
    {
        [self.delegate menuTapped:1];
    }
    else
    {
        [self.delegate menuTapped:2];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)setMenu1ImageView:(ResponsiveImageView *)imageView
{
    _menu1ImageView = imageView;
    self.menu1ImageView.userInteractionEnabled = YES;
    self.menu1ImageView.delegate = self;
    
}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)setMenu2ImageView:(ResponsiveImageView *)imageView2
{
    _menu2ImageView = imageView2;
    self.menu2ImageView.userInteractionEnabled = YES;
    self.menu2ImageView.delegate = self;
    
}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)setMenu1Label:(ResponsiveLabel *)menu1Label
{
    _menu1Label = menu1Label;
    _menu1Label.userInteractionEnabled = YES;
    _menu1Label.delegate = self;
}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)setMenu2Label:(ResponsiveLabel *)menu2Label
{
    _menu2Label = menu2Label;
    _menu2Label.userInteractionEnabled = YES;
    _menu2Label.delegate = self;
}

//////////////////////////////////////////////////////////////////////////////////////////


@end
