#import "ViewController.h"
#import "TSAnimatedSidebar.h"

@interface ViewController () <TSAnimatedSidebarDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;



@end

//////////////////////////////////////////////////////////////////////////////////////////

@implementation ViewController
{
    NSMutableArray* _animationFrames;
    NSInteger _frameIndex;
    NSTimer* _periodic;
    
    TSAnimatedSidebar* sidebar;
}

//////////////////////////////////////////////////////////////////////////////////////////
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    _frameIndex = 0;
    _animationFrames = [NSMutableArray new];
    
    //init the frames array
    [_animationFrames addObject:[UIImage imageNamed:@"frame_21"]];
    [_animationFrames addObject:[UIImage imageNamed:@"frame_22"]];
    [_animationFrames addObject:[UIImage imageNamed:@"frame_23"]];
    [_animationFrames addObject:[UIImage imageNamed:@"frame_24"]];
    [_animationFrames addObject:[UIImage imageNamed:@"frame_25"]];
    [_animationFrames addObject:[UIImage imageNamed:@"frame_26"]];
    
}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)viewWillAppear:(BOOL)animated
{
    UIBarButtonItem* optionsButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"frame_21"] landscapeImagePhone:nil style:UIBarButtonItemStylePlain target:self action:@selector(optionsButtonTapped)];
    
    self.navigationItem.rightBarButtonItem = optionsButton;

}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)optionsButtonTapped
{
    if (nil != sidebar)
    {
        [sidebar close];
        [self closeOptionsMenuWithAnimation];
        return;
    }
    
    sidebar = [TSAnimatedSidebar new];
    sidebar.delegate = self;
    
    sidebar.image1StartName = @"flat_circle";
    sidebar.image2StartName = @"flat_circle_red";
    sidebar.menu1Label = @"VIEWED";
    sidebar.menu2Label = @"SHOWCASED";
    sidebar.animationSpeedFactor = 0.3;
    
    [sidebar.view setFrame:self.imageView.frame];
    [self.view addSubview:sidebar.view];
    
    [self openOptionsMenuWithAnimation];

}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)animateButtonForward
{
    [self animateButton:YES];
}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)animateButtonBackwards
{
    [self animateButton:NO];
    
}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)animateButton:(BOOL)forward
{
    if (forward)
    {
        _frameIndex++;
        if (_frameIndex > 5)
        {
            [_periodic invalidate];
            _periodic = nil;
            return;
        }
    }
    else
    {
        _frameIndex--;
        if (_frameIndex < 0)
        {
            [_periodic invalidate];
            _periodic = nil;
            return;
        }
    }
    
    UIBarButtonItem* button = self.navigationItem.rightBarButtonItem;
    [button setImage:_animationFrames[_frameIndex]];
}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)menuItemTapped:(NSInteger)index
{
    NSLog(@"menu tapped: %d", index);
    [self closeOptionsMenuWithAnimation];

}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)closeOptionsMenuWithAnimation
{
    [_periodic invalidate];
    _periodic = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(animateButtonBackwards) userInfo:nil repeats:YES];
   
}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)openOptionsMenuWithAnimation
{
    [_periodic invalidate];
    _periodic = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(animateButtonForward) userInfo:nil repeats:YES];
    
}

//////////////////////////////////////////////////////////////////////////////////////////

- (void)didFinishAnimatingOut
{
    [sidebar.view removeFromSuperview];
    sidebar = nil;
}

//////////////////////////////////////////////////////////////////////////////////////////

@end
